# Installation for HacDC

Last edited by Ubuntourist (ubuntourist@hacdc.org) 2019.06.27

According to `lsusb` the Ultimaker 2 identifies itself as:

    Bus 001 Device 006: ID 2341:0010 Arduino SA Mega 2560 (CDC ACM)

and it appears in `/dev` as `/dev/ttyACM0` (at the moment).

----

## Backup the flash memory:

I will assume that it is similar to the [TAZ
procedure](https://gitlab.com/ubuntourist/lulzbot-taz-1.0-marlin-firmware)...


    $ avrdude -C /etc/avrdude.conf -p m2560 -c wiring -P /dev/ttyACM0 -b 115200 -U flash:r:flash-2019-06-27.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9801 (probably m2560)
    avrdude: reading flash memory:
    
    Reading | ################################################## | 100% 31.68s
    
    avrdude: writing output file "flash-2019-06-27.hex"
    
    avrdude: safemode: Fuses OK (E:FD, H:D0, L:FF)
    
    avrdude done.  Thank you.

----

## Backup the EEPROM:

Alas, it is not so for the EEPROM...

    $ avrdude -C /etc/avrdude.conf -p m2560 -c wiring -P /dev/ttyACM0 -b 115200 -U eeprom:r:eeprom-2019-06-27.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9801 (probably m2560)
    avrdude: reading eeprom memory:
    
    Reading |                                                    | 0% 0.00savrdude: stk500v2_command(): command failed
    avrdude: stk500v2_paged_load: read command failed
    avrdude: stk500v2_command(): command failed
    avrdude: stk500v2_paged_load: read command failed
    avr_read(): error reading address 0x0000
        read operation not supported for memory "eeprom"
    avrdude: failed to read all of eeprom memory, rc=-2
    
    avrdude: safemode: Fuses OK (E:FD, H:D0, L:FF)
    
    avrdude done.  Thank you.

----

The discussion at [Tinker firmware -
introduction](https://community.ultimaker.com/topic/15939-tinker-firmware-introduction/page/3/)
may prove useful...

----

## Testing...

I probably should have done this BEFORE wiping the flash, for
comparison's sake...

    $ pronsole.py 
    Welcome to the printer console! Type "help" for a list of available commands.
    offline> connect /dev/ttyACM0
    start
    Printer is now online
    Marlin 1.0.0 
    Last Updated: Jun 27 2019 21:27:37 | Author: Version DEV
    Compiled: Jun 27 2019
    Free Memory: 2247  PlannerBufferBytes: 1232
    Stored settings retrieved
    Steps per unit:
    M92 X80.00 Y80.00 Z200.00 E282.00
    Maximum feedrates (mm/s):
    M203 X300.00 Y300.00 Z40.00 E45.00
    Maximum Acceleration (mm/s2):
    M201 X9000 Y9000 Z100 E10000
    Acceleration: S=acceleration, T=retract acceleration
    M204 S3000.00 T3000.00
    Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum XY jerk (mm/s),  Z=maximum Z jerk (mm/s),  E=maximum E jerk (mm/s)
    M205 S0.00 T0.00 B20000 X20.00 Z0.40 E5.00
    Home offset (mm):
    M206 X0.00 Y0.00 Z-14.45
    PID settings:
    M301 P10.00 I2.50 D100.00
    SD card ok 
    ttyACM0 PC>

----

